//---------------------------------- DOM manipulator-------------------------------
// references : http://api.jquery.com/category/manipulation/
$("selector").addClass();
$("selector").after();
$("selector").appendTo();
$("selector").appendTo();
$("selector").before();
$("selector").clone();
$("selector").css();
$("selector").detach();
$("selector").empty();
$("selector").hasClass();
$("selector").height();
$("selector").html();
$("selector").innerHeight();
$("selector").innerWidth();
$("selector").insertAfter();
$("selector").insertBefore();
$("selector").offset();
$("selector").outerHeight();
$("selector").outerWidth();
$("selector").position();
$("selector").prepend();
$("selector").prependTo();
$("selector").prop();
$("selector").remove();
$("selector").removeAttr();
$("selector").removeClass();
$("selector").removeProp();
$("selector").replaceAll();
$("selector").replaceWith();
$("selector").scrollLeft();
$("selector").scrollTop();
$("selector").text();
$("selector").toggleClass();
$("selector").unwrap();
$("selector").val();
$("selector").width();
$("selector").wrap();
$("selector").wrapAll();
$("selector").wrapInner();
//---------------------------------- end DOM manipulator-------------------------------

// --------------------------------  jquery event listener ------------------------------------
// references: https://api.jquery.com/category/events/
$("selector").bind();
$("selector").on();
$("selector").change();
$("selector").click();
$("selector").dblclick();
$("selector").focus();
$("selector").focusin();
$("selector").focusout();
$("selector").hover();
$("selector").keydown();
$("selector").keypress();
$("selector").keyup();
$("selector").mousedown();
$("selector").mouseenter();
$("selector").mouseleave();
$("selector").mousemove();
$("selector").mouseout();
$("selector").mouseover();
$("selector").mouseup();
$("selector").off();
$("selector").resize();
$("selector").scroll();
$("selector").select();
$("selector").submit();
$("selector").toggle();
$("selector").unbind();
//---------------------------------- end ------------------------------------------------

//---------------------------------- Looping Through a Set of Selected Results-------------------------------
var urls = [];
$("#url-list li a[href]").each(function(i, object) {
	urls[i] = $(this).attr('href');
});
console.log(urls); 
//---------------------------------- end ------------------------------------------------

// -------------------------------- Process selected elements ------------------------------------
var months = [ 'January', 'February', 'March', 'April', 'May',
'June', 'July', 'August', 'September', 'October',
'November', 'December'];

$.each(months, function(index, value) {
	$('#months').append('<li>' + value + '</li>');
});
//---------------------------------- end ------------------------------------------------

// -------------------------------- Filtering Arrays ------------------------------------
// filter month by first char = 'J'
var months = [ 'January', 'February', 'March', 'April', 'May',
							'June', 'July', 'August', 'September', 'October',
							'November', 'December'];
months = $.grep(months, function(value, i) {
	return ( value.indexOf('J') === 0 );
});
$('#months').html( '<li>' + months.join('</li><li>') + '</li>' );

// selecting even month only (bulan genap)
var months = [ 'January', 'February', 'March', 'April', 'May',
							'June', 'July', 'August', 'September', 'October',
							'November', 'December'];
months = $.grep(months, function(value, i) {
	return ( i % 2 ) === 0;
});
$('#months').html( '<li>' + months.join('</li><li>') + '</li>' );
//---------------------------------- end ------------------------------------------------

// -------------------------------- Iterating and Modifying Array Entries ------------------------------------
// modifying javascript array
// wil; result 'January Yeah', 'February Yeah', etc
var months = [ 'January', 'February', 'March', 'April', 'May',
							'June', 'July', 'August', 'September', 'October',
							'November', 'December'];
months = $.map(months, function(value, i) {
	return value + ' Yeah';
});
$('#months').html( '<li>' + months.join('</li><li>') + '</li>' );
//---------------------------------- end ------------------------------------------------

// -------------------------------- Combining Two Arrays ------------------------------------
var horseBreeds = ['Quarter Horse', 'Thoroughbred', 'Arabian'];
var draftBreeds = ['Belgian', 'Percheron'];
var breeds = $.merge( horseBreeds, draftBreeds );
$('#horses').html( '<li>' + breeds.join('</li><li>') + '</li>' );
//---------------------------------- end ------------------------------------------------

// -------------------------------- Attaching Objects and Data to DOM ------------------------------------
var person = {
	id: 24,
	name: 'Rifan MF',
	address: 'Antapani Mas No 39 A',
	phoneNumber: '022-2348576',
};
$("p#person").data(person);
// will result 
// <p id="person" data-id="24" data-name="Rifan MF" 
// 		data-address="Antapani Mas No 39 A" data-phone-number="022-2348576"></p>

//---------------------------------- end ------------------------------------------------

// --------------------------------  Extending Objects ------------------------------------
var person = {
	id: 24,
	name: 'Rifan MF',
};
var person2 = {
	id: 25,
	style: 'cool',
};
var person3 = $.extend(person, person2);
/** 
will result 
person3 = 
	{
		id: 25,
		name: 'Rifan MF',
		style: 'cool',
	}
**/
//---------------------------------- end ------------------------------------------------
