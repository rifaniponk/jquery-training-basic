// variable declaration
// dilarang mengubah apapun yang disini
var persons = [
	{
		id: 1,
		name: 'Rifan MF',
	},
	{
		id: 2,
		name: 'Yunero',
	},
	{
		id: 3,
		name: 'Dadang Konelo',
	},
	{
		id: 4,
		name: 'Suparto Mandigan',
	},
	{
		id: 5,
		name: 'Yuki',
	}
];

var additions = [
	{
		id: 8,
		name: 'Sukimin Yakusa'
	},
	{
		id: 9,
		name: 'Wanebo Alay'
	},
	{
		id: 10,
		name: 'Juminten'
	}
];

var extraInfo = [
	{
		id: 1,
		address: 'Antapani Mas 39',
	},
	{
		id: 2,
		address: 'Jalan Jakarta 29',
	},
	{
		id: 4,
		address: 'Kuningan Raya RT 67',
	},
	{
		id: 5,
		address: 'Komplek Pratista No 45',
	},
	{
		id: 9,
		address: 'Jalan Ciwaduk Naga No 78'
	},
	{
		id: 10,
		address: 'Jalan Sumatra No 90'
	}
];

var users = [
	{id: 1, name: "Axel", email: "massa.Quisque.porttitor@sociis.com", phone: "(03) 8921 5700", country: "Vanuatu"},
	{id: 2, name: "Venus", email: "lorem.luctus.ut@Crasdolordolor.co.uk", phone: "(03) 1432 2157", country: "Norfolk Island"},
	{id: 3, name: "Cassandra", email: "volutpat.Nulla.facilisis@nec.com", phone: "(03) 2306 5894", country: "British Indian Ocean Territory"},
	{id: 4, name: "Noelle", email: "et.malesuada.fames@Maecenas.net", phone: "(07) 0772 8128", country: "Djibouti"},
	{id: 5, name: "Alden", email: "pede.nec.ante@metus.org", phone: "(03) 2142 7318", country: "Rwanda"},
	{id: 6, name: "Fallon", email: "a.malesuada.id@scelerisqueneque.ca", phone: "(06) 9645 6045", country: "Bahamas"},
	{id: 7, name: "Tamara", email: "pede@Nullamfeugiatplacerat.net", phone: "(01) 8802 5790", country: "Korea, South"},
	{id: 8, name: "Oleg", email: "non@velit.edu", phone: "(04) 4991 5273", country: "Bangladesh"},
	{id: 9, name: "Kenyon", email: "id@sedpede.com", phone: "(05) 5127 3333", country: "Lithuania"},
	{id: 10, name: "Slade", email: "odio.vel.est@cursus.ca", phone: "(03) 8094 7518", country: "San Marino"},
	{id: 11, name: "Martin", email: "Curabitur.consequat@augueeu.co.uk", phone: "(05) 0299 7759", country: "Saint Pierre and Miquelon"},
	{id: 12, name: "Xandra", email: "quis@pedenecante.com", phone: "(09) 1363 2545", country: "Canada"},
	{id: 13, name: "Jena", email: "lorem@montes.edu", phone: "(03) 8222 9449", country: "Iceland"},
	{id: 14, name: "Hayes", email: "litora.torquent@diam.net", phone: "(04) 3793 5054", country: "Sao Tome and Principe"},
	{id: 15, name: "Reagan", email: "malesuada@nonarcu.org", phone: "(03) 2402 2336", country: "South Georgia and The South Sandwich Islands"},
	{id: 16, name: "Yeo", email: "Nullam.velit@dis.edu", phone: "(09) 9174 3402", country: "Mauritania"},
	{id: 17, name: "Tanya", email: "hendrerit.neque@eutelluseu.com", phone: "(09) 2459 1771", country: "Jersey"},
	{id: 18, name: "Yvette", email: "leo@enimMaurisquis.net", phone: "(03) 1239 7507", country: "Viet Nam"},
	{id: 19, name: "Jasmine", email: "est.Mauris@at.edu", phone: "(08) 7345 1222", country: "Ireland"},
	{id: 20, name: "Preston", email: "consectetuer.cursus.et@Cras.org", phone: "(07) 4394 7662", country: "Uganda"},
];

// =====================================================================
