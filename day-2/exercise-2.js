$(function(){
// HINT: 
// semua soal akan menggunakan variable yang ada di 'variable-declaration-for-exercise-2.js'
// RULES:
// dilarang mengubah 'variable-declaration-for-exercise-2.js' dan 'exercise-2.html'
// ==================================================================================
/** -------------------------- SOAL-1 ------------------------------------
soal #1 - A
- tampilkan semua nama dan id yang ada pada variable persons kedalam table soal #1 - A
- berikan kelas '.info' pada setiap baris genap di soal #1 - A
- berikan tag <b> untuk nama 'Dadang Konelo'. sehingga menjadi <b>Dadang Konelo</b>
soal #1 - B
- gabungkan variable persons dan addition sehingga menjadi sebuah array baru dengan anggota yg lbh banyak
- tampilkan id, nama, dan adress pada variable gabungan tersebut
	(untuk address silakan mengambil dari extraInfo berdasarkan id yang sesuai)
	hint: 
		gunakan $.merge untuk menggabungkan array
		gunakan $.map untuk membuat sebuah array person dengan atribut id, name, dan address
		gunakan $.grep untuk mengambil object di extraInfo,
		gunakan $.extend untuk menggabungkan properti di object
- bila ada address tidak ada (id 3 & id 8), maka tampilkan 'no address'
- berikan tag <i> dan warna merah untuk semua kolom 'no address'
-------------------------------------------------------------------------**/
// your code here




/** -------------------------- END SOAL-1 ------------------------------ */
// ====================================================================================================
/** --------------------------- SOAL-2 ---------------------------------
- tampilkan atribut name, phone, dan country (dari variable users) pada table Soal #2
- masukan data-id ke setiap row dengan menggunakan .data('id', value)
- untuk kolom action. tampilkan <button class="btn btn-info btn-xs btn-edit">update</button>
- tambahkan kelas 'success' pada setiap row (<tr>) ketika mouse berada diatasnya (hover) 
- jika kita mengklik pada sebuah row. maka kelas row tersebut akan menjadi 'warning'
	dan semua row yang lainnya akan menjadi normal kembali (remove class warning)
- jika kita mengklik button 'update' maka semua user atribut akan dimunculkan di form.
	masukan juga atribut id ke hidden input yang ada di form
- jika kita mengubah value yg ada di form lalu klik submit. maka data di table pun akan berubah
------------------------------------------------------------------------ */
// your code here



/** -------------------------- END SOAL-2 ------------------------------ */
});