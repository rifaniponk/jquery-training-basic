$("#soal-1 tr:hidden").show();
$("#soal-1 tbody tr:eq(5)").hide();
$("#soal-1").addClass('table-bordered');
$("#soal-1 tbody tr:even").addClass('active');
$("#soal-1 tbody tr td:nth-child(3)").prepend('telp: ');
$("#soal-1 td:contains(Rifan), #soal-1 td:contains(Magnus), #soal-1 td:contains(Pratista)").css('color', 'red');
$("#soal-1 td:contains(Naga hitam)").append(' RT 05 RW 07').addClass('warning');
$("#soal-1 td:contains(Sujarwo)").css('color', 'green').next().next().css('color', 'green');
$("#soal-1 tbody tr:eq(3) td:not(:eq(3))").addClass('success');

$(":text:eq(0)").val('Andi');
$(":password:eq(0)").val('andi');
$("input[type=email]:eq(0)").val('andi.sunardi');
$("select:eq(0)").val(2);
$(":radio:eq(0)").attr('checked', 'checked');
$("textarea:eq(0)").val('saya sekolah di Bandung');
$(":text:eq(1)").attr('placeholder', 'last name');
$("textarea:eq(1)").attr('disabled', 'disabled');
$(":submit").removeClass('btn-primary').addClass('btn-success');
$(".form-group:not(:eq(7),:eq(4))").addClass('has-success')
			.find('.col-md-4:has(:text)')
			.append('<span class="help-block">Isilah adalah input text</span>')
			.parent().parent()
			.find('.col-md-4:has(input[type=email])')
			.append('<span class="help-block">kalo ini email</span>');