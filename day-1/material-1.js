//---------------------------------- 1 jquery basic syntax-------------------------------
// would be executed once the 
// DOM is ready to be traversed and manipulated
$(document).ready(function(){
    // jquery code
});

// shorter syntax
$(function(){
    // jquery code
});
//---------------------------------- end 1 ------------------------------------------------


//---------------------------------- 2 selecting by id/class -------------------------------
$("#navbar"); // select element with id='navbar'
$(".list-item"); // select element with class='list-item'

$("div#navbar"); // select <div> with id='navbar'
$("ul.list-item"); // select <ul> with class='list-item'
//---------------------------------- end 2 -----------------------------------------------

//----------------------------------  3 selecting child element -------------------------------
// select all <a> inside <li> inside .list-item inside #navbar
$("#navbar ul.list-item li a"); 

// select direct child <a> inside #navbar
$("#navbar ul.list-item li > a");
$("#navbar ul.list-item li").children('a'); 

// get all children .list-item
$("#navbar ul.list-item > *");
$("#navbar ul.list-item").children();
$("#navbar ul.list-item").find("> *");
//---------------------------------- 3 end  ------------------------------------------------

//----------------------------------  4 selecting element by order -------------------------------
$("ul li:first"); // Matches the first selected element
$("ul li:last"); // Matches the last selected element
$("ul li:even"); // Matches even elements (zero-indexed)
$("ul li:odd");  // Matches even elements (zero-indexed)
$("ul li:eq(n)"); // Matches a single element by its index (n)  
$("ul li:lt(n)"); //  Matches all elements with an index below n
$("ul li:gt(n)"); // Matches all elements with an index above n
$("ul li:nth-child(n)"); // matches every element that is the n-th child
//---------------------------------- 4 end  ------------------------------------------------

//---------------------------------- 5 Selecting Elements Based on What They Contain  -------------------------------
$('span:contains("Bob")'); // select <span> which contains Bob

// match all <div>elements that encapsulate <a>elements (anchors) 
// within <p>elements (paragraphs).
$('div:has(p a)');
//---------------------------------- 5 end  ------------------------------------------------

//---------------------------------- 6 Selecting Elements by What They Don’t Match  -------------------------------
// Select all <a> inside #nav except <a> with class 'content'
$('#nav a:not(.active)'); 
$('#nav a').not('a.active'); 
//---------------------------------- end  ------------------------------------------------

//----------------------------------  7 Selecting Elements Based on Their Visibility  -------------------------------
$('div:hidden'); // match all hidden div
//---------------------------------- 7 end  ------------------------------------------------

//----------------------------------  8 Selecting Elements Based on Attributes -------------------------------
$('li[class]'); // match all <li> which has attribute class
$('li[class=active]'); // match all <li> which class='active'
$('li[class!=active]'); // match all <li> except <li> with class='active'
$('li[class^=act]'); // match all <li> which class start with 'act'
$('li[class$=tive]'); // match all <li> which class end with 'act'
$('li[class~=active]'); // match all <li> which class='active' but not class='actived'
//---------------------------------- 8 end  ------------------------------------------------


//---------------------------------- 9 Selecting Form Elements by Type  -------------------------------
$(':text');     // match <input type="text" />
$(':password'); // match <input type="password" />
$(':radio');    // match <input type="radio" />
$(':checkbox'); // match <input type="checkbox" />
$(':submit');   // match <input type="submit" />
$(':image');    // match <input type="image" />
$(':reset');    // match <input type="reset" />
$(':button');   // match <input type="button" />
$(':file');     // match <input type="file" />
$(':hidden');   // match <input type="hidden" />
//---------------------------------- 9 end  ------------------------------------------------


//----------------------------------   -------------------------------
//---------------------------------- end  ------------------------------------------------







