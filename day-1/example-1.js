// would be executed once the 
// DOM is ready to be traversed and manipulated
$(document).ready(function(){
    // jquery code
});

// shorter syntax
$(function(){
    // jquery code here
    
    $("ul#list").addClass('list-style'); // ul with id: list-style
    $("ul#list li.red").css('color', 'red'); // li with class: red
    $("ul#list li:eq(4)").css('color', 'blue'); // li with index: 4
    $("ul#list li:eq(4)").next().css('color', 'green'); // next element
    $("ul#list li:eq(4)").prev().css('color', 'orange'); // prev element

    $("ul#list li:nth-child(2)").css('backgroundColor', 'yellow');

    $("ul#list2 li:first").css('font-size', '20px')
    					.text('list 21 - text telah diganti dari js'); // first li in #list2
    $("ul#list2 li:last").css('font-size', '20px'); // last li in #list2
    $("ul#list2").find("li:eq(2)").append('   <small>sisipan text</small>');
    $("ul#list2 li:eq(3)").prepend('<small>sisipan text</small>      ');

    $("ul#list3 li > a").addClass('btn btn-default'); // descendant combinator ( > )
    $("ul#list3 li").children('a').addClass('btn btn-primary'); 
    $("ul#list3 li").find('> a').addClass('btn btn-success'); 

    $("input[type=text]").val('ini value dari js');

});


