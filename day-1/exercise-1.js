
// ================== exercise-1 GOAL ======================================
// ---------------------- soal 1 --------------------------------------------
// 1. Tampilkan baris No 7. 'Sujarwo'
// 2. Sembunyikan baris No 6. 'Intaro Sukim'
// 3. tambahkan class 'table-bordered' di element <table>
// 4. Buat table menjadi zebra-style dengan menerapkan tr class=active pada setiap baris ganjil
// 5. tambahkan prepend 'telp:'' pada semua nomor telp, sehingga menjadi 'telp: 0987263829'
// 6. ubah warna text 'Rifan M F', 'Jalan Magnus Api', dan 'Pratista Raya No 57' menjadi merah
// 7. tambahkan append 'RT 05 RW 07' pada 'Jalan Naga hitam' lalu tambahkan class 'warning' pada kolomnya
// 8. gunakan jquery next() untuk membuat 'Sujarwo' dan 'Jalan Naga Hitam' menjadi warna hijau
// 9. tambahkan class 'success' pada semua kolom di baris No. 4 'sutarman', kecuali kolom address. (gunakan :not)
// ------------------------ soal 2 --------------------------------------------------
// 1. ubah input value nama menjadi ='Andi'
// 2. ubah input value kata sandi menjadi ='andi'
// 3. ubah input value email menjadi ='andi.sunardi'
// 4. ubah default option pendidikan menjadi ='SMP'
// 5. ubah default jenis kelamin menjadi 'pria'
// 6. ubah  biografi menjadi 'saya sekolah di Bandung'
// 7. ubah placeholder Nama Belakang menjadi 'last name'
// 8. Ubah alamat menjadi disable
// 9. ubah kelas btn-primary menjadi btn-success
// 10. tambahkan kelas 'has-success' pada semua .form-group kecuali pendidikan dan alamat.
//		setelah itu berikan '<span class="help-block">Isilah nama dengan benar</span>' di bawah semua input yang bertipe text
//		dan '<span class="help-block">kalo ini email</span>' di bawah input yang bertipe email
// -------------------------------------------------------------------------------------
// ------------------------- RULES ------------------------------------------
// 1. dilarang mengubah html file. gunakan jquery code untuk menyelesaikan semua soal
// 2. hanya diperbolehkan menggunakan jquery selector. looping, if else, dll tidak diperbolehkan 
// 3. setiap soal hanya boleh menggunakan 1 baris code (satu semicolon ';');
// ==========================================================================

// add your code here
$().addClass('class-name').next().addClass;
$().show();
$().hide();
